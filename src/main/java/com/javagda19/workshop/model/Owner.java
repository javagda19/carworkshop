package com.javagda19.workshop.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Owner implements IBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String lastname;
    private String PESEL;

    // jeśli będę chciał się dowiedzieć jakie samochody ma
    // dany właściciel, to powinienem zrobić relację zwrotną
    //
    // Jeśli macie więcej niż jedną relację to nie możecie zastosować relacji z List z EAGER
    @ToString.Exclude
    @OneToMany(mappedBy = "owner", fetch = FetchType.EAGER)
    private List<Car> cars = new ArrayList<>();

    public Owner(String name, String lastname, String PESEL) {
        this.name = name;
        this.lastname = lastname;
        this.PESEL = PESEL;
    }

    public Owner(Long id) {
        this.id = id;
    }

}
