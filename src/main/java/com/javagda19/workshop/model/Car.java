package com.javagda19.workshop.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Car implements IBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String registrationNumber;
    private String manufacturer;
    private String model;
    private int productionYear;

    // czytamy : wiele samochodów do jednego właściciela.
    @ManyToOne
    private Owner owner;

    @ToString.Exclude
    @OneToMany(mappedBy = "car", fetch = FetchType.EAGER)
    private Set<RepairOrder> repairOrders;

    @ManyToMany(mappedBy = "carSet", fetch = FetchType.EAGER)
    private Set<Mechanic> mechanics;
    // mechanics who have been repairing me

    public Car(String registrationNumber, String manufacturer, String model, int productionYear) {
        this.registrationNumber = registrationNumber;
        this.manufacturer = manufacturer;
        this.model = model;
        this.productionYear = productionYear;
    }

}
