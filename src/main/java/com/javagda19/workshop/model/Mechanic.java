package com.javagda19.workshop.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Mechanic implements IBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String lastname;

//    @Enumerated(value = EnumType.ORDINAL) - numer/indeks pozycji w enum
    @Enumerated(value = EnumType.STRING) // - wartość string enuma
    private MechanicPosition position;

    @ManyToMany
    @ToString.Exclude
    private Set<Car> carSet; // cars i have worked on
}
