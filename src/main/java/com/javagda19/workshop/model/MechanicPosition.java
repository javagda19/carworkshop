package com.javagda19.workshop.model;

public enum MechanicPosition {
    JUNIOR, // pozycja - 0
    MID,    // pozycja - 1
    SENIOR, // pozycja - 2
    JANUSZ; // pozycja - 3

    // String value jest
    // identyczny jak nazwa
    // enuma

}
