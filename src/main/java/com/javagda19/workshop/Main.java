package com.javagda19.workshop;

import com.javagda19.workshop.logic.*;
import com.javagda19.workshop.model.Car;
import com.javagda19.workshop.model.Owner;
import com.javagda19.workshop.model.RepairOrder;


import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {
        EntityDao entityDao = new EntityDao();

        Scanner scanner = new Scanner(System.in);

        boolean working = true;

        do {
            String linia = scanner.nextLine();

            if (linia.equalsIgnoreCase("quit")) {
                working = false;
            } else {
                String[] slowa = linia.split(" ");

                String komenda = slowa[0];

                if (komenda.equalsIgnoreCase("dodaj") && slowa[1].equalsIgnoreCase("ownera")) {
                    // tutaj logika dodawania owner'a
                    addOwner(entityDao, slowa);
                } else if (komenda.equalsIgnoreCase("dodaj") && slowa[1].equalsIgnoreCase("car")) {
                    // tutaj logika dodawania car
                    addCar(entityDao, slowa);
                } else if (komenda.equalsIgnoreCase("list") && slowa[1].equalsIgnoreCase("car")) {
                    // tutaj logika listowania car
                    List<Car> cars = entityDao.getListOfAll(Car.class);

                    cars.forEach(System.out::println);
                } else if (komenda.equalsIgnoreCase("list") && slowa[1].equalsIgnoreCase("owner")) {
                    // tutaj logika listowania ownerów
                    List<Owner> owners = entityDao.getListOfAll(Owner.class);

                    owners.forEach(System.out::println);
                }else if(komenda.equalsIgnoreCase("powiaz")){
                    Long ownerId = Long.parseLong(slowa[1]);
                    Long carId = Long.parseLong(slowa[2]);

                    Optional<Owner> ownerOptional = entityDao.getById(Owner.class, ownerId);
                    Optional<Car> carOptional = entityDao.getById(Car.class, carId);

                    if(ownerOptional.isPresent() && carOptional.isPresent()){
                        Owner owner = ownerOptional.get();
                        Car car = carOptional.get();

                        owner.getCars().add(car);
                        car.setOwner(owner);

                        entityDao.save(car);
                        entityDao.save(owner);
                    }else{
                        System.err.println("Błąd, nie mogę znaleźć obiektu.");
                    }
                }else if(komenda.equalsIgnoreCase("dodaj_order")){
                    // dodaj_order car_id
                    // dodaj_order 6
                    // 1. Stwórz obiekt RepairOrder.
                    RepairOrder repairOrder = new RepairOrder();

                    // 2. Po stworzeniu obiektu zapisz go w bazie (użyj entityDao.save)
                    entityDao.save(repairOrder);

                    // 3. Przyjmij od użytkownika (gdzieś w parametrach, czyli np. zakładamy że
                    //  będzie to 2 prametr) car_id
                    Long carId = Long.parseLong(slowa[1]);

                    // 4. Pobierz z bazy samochód, powiąż go z RepairOrder.
                    Optional<Car> carFromDatabase = entityDao.getById(Car.class, carId);
                    if(carFromDatabase.isPresent()){
                        Car car = carFromDatabase.get();

                        car.getRepairOrders().add(repairOrder);
                        repairOrder.setCar(car);

                        // 5. Zapisz RepairOrder oraz samochód w bazie
                        entityDao.save(car);
                        entityDao.save(repairOrder);
                    }else{
                        System.err.println("Unable to find car.");
                    }
                }else if( komenda.equalsIgnoreCase("add_order")){
                    // Tadaaaa!
                    RepairOrderManager repairOrderManager = new RepairOrderManager();
                    repairOrderManager.createRepairOrderForCar(Long.parseLong(slowa[1]));
                }else if(komenda.equalsIgnoreCase("dodaj") && slowa[1].equalsIgnoreCase("mechanic")){
                    MechanicManager mechanicManager = new MechanicManager();
                    mechanicManager.saveMechanic(slowa[2], slowa[3], slowa[4].toUpperCase());
                }else if(komenda.equalsIgnoreCase("do_roboty")){
                    // przypisanie mechanika do samochodu
                    MechanicManager mechanicManager = new MechanicManager();
                    mechanicManager.assignCarToMechanic(Long.parseLong(slowa[1]), Long.parseLong(slowa[2]));
                }else if(komenda.equalsIgnoreCase("wypiszMechanikow")){
                    CarManager carManager = new CarManager();
                    carManager.wypiszMechanikow(Long.parseLong(slowa[1]));
                }

            }

        } while (working);


        HibernateUtil.getSessionFactory().close();
    }

    private static void addCar(EntityDao entityDao, String[] slowa) {
        String registrationNumber = slowa[2]; // można stworzyć alias, tutaj np. dla parametru 'registration number'

        Car car = new Car(registrationNumber, slowa[3], slowa[4], Integer.parseInt(slowa[5]));
        entityDao.save(car);
    }

    private static void addOwner(EntityDao entityDao, String[] slowa) {
        String name = slowa[2]; // alias dla name, stworzyłem zmienną żeby nie używać bardzo nieczytelnego zapisu slowa[2];

        Owner owner = new Owner(name, slowa[3], slowa[4]); // imie, nazwisko, pesel

        entityDao.save(owner);
    }
}
