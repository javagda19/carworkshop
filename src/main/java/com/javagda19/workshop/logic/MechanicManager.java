package com.javagda19.workshop.logic;

import com.javagda19.workshop.model.Car;
import com.javagda19.workshop.model.Mechanic;
import com.javagda19.workshop.model.MechanicPosition;
import com.javagda19.workshop.model.RepairOrder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.TransactionException;

import java.sql.SQLException;

public class MechanicManager {

    public void saveMechanic(String name, String lastName, String position) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        Transaction transaction = null;
        Session session = sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();

            // Tworzymy mechanika (można stworzyć oddzielny konstruktor zamiast używać setterów)
            Mechanic mechanic = new Mechanic();
            mechanic.setLastname(lastName);
            mechanic.setName(name);
            mechanic.setPosition(MechanicPosition.valueOf(position));

            // zapis mechanika w bazie
            session.save(mechanic);

            transaction.commit();
        } catch (TransactionException sqle) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void assignCarToMechanic(Long carId, Long mechanicId) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        Transaction transaction = null;
        Session session = sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();

            // logika logika logika
            Car car = session.get(Car.class, carId);
            Mechanic mechanic = session.get(Mechanic.class, mechanicId);

            // dodanie relacji
            car.getMechanics().add(mechanic);
            mechanic.getCarSet().add(car);

            // zapis do bazy.
            session.update(car);
            session.update(mechanic);

            transaction.commit();
        } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
