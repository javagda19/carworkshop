package com.javagda19.workshop.logic;

import com.javagda19.workshop.model.Car;
import com.javagda19.workshop.model.Mechanic;
import com.javagda19.workshop.model.MechanicPosition;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.TransactionException;

public class CarManager {
    public void wypiszMechanikow(Long carId){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        Session session = sessionFactory.openSession();
        Car car = null;
        try {

            car = session.get(Car.class, carId);

//          car.getMechanics().forEach(System.out::println);

        } catch (TransactionException sqle) {
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        car.getMechanics().forEach(System.out::println);
    }
}
