package com.javagda19.workshop.logic;

import com.javagda19.workshop.model.Car;
import com.javagda19.workshop.model.RepairOrder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class RepairOrderManager {
    public void createRepairOrderForCar(Long carId) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        Transaction transaction = null;
        Session session = sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();

            // logika logika logika...
            // 1. stworzenie repair order
            RepairOrder repairOrder = new RepairOrder();

            // 2. zapisanie repair order
//            session.save(repairOrder);

            // 3. pobranie samochodu z identyfikatorem carId
            Car car = session.get(Car.class, carId);
            // Gdyby samochodu nie udało się odnaleźć otrzymalibyśmy exception
            // co spowodowałoby wykonanie rollback'a.

            repairOrder.setCar(car);
            car.getRepairOrders().add(repairOrder);

            session.update(car);
//            session.update(repairOrder);
            session.save(repairOrder);

            transaction.commit();
        } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
